import { configureStore, createSlice } from 'redux-starter-kit'
import rootReducer from './reducers'


// function setValue(major, minor, value) {
//     return {
//       type: 'SET_VALUE',
//       payload: { 
//         major, minor, value
//     }
//     }
//   }

// const setValue = createAction('SET_VALUE');

const sudokuReducer = {
    "SET_VALUE": (state, action) => {
        return state.concat(action.payload.value)
     }
}

const sudokuSlice = createSlice({
    initialState: new Array(9).fill(new Array(9).fill(null)),
    reducers:{
        setValue(){}
    }
})

// function sudokuReducer(state = [], action) {
//     switch(action.type) {
//         case "SET_VALUE": {
//             debugger;
//             return state.concat(action.payload);
//         }
//         // case "TOGGLE_TODO": {
//         //     const {index} = action.payload;
//         //     return state.map( (todo, i) => {
//         //         if(i !== index) return todo;

//         //         return {
//         //             ...todo,
//         //             completed : !todo.completed
//         //         };
//         //     });
//         // }
//         // case "REMOVE_TODO": {
//         //     return state.filter( (todo, i) => i !== action.payload.index)
//         // }
//         default : return state;
//     }
// }

export const store = configureStore({
  reducer: {
    sudokuReducer
  }
});
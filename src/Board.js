import React from 'react';
import './Board.css';

class MinorBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            'number': props.number
        };
        this.increment = this.increment.bind(this)
    }

    increment(event){
        switch (this.state.number){
            case "":
                this.setState({'number': 0});
                break;
            case 9:
                this.setState({'number': ""});
                break;
            default:
                this.setState({'number': (this.state.number + 1) % 10});
                break;

            
        } 
    }

    render() {
        return <div className="MinorBlock" ><button value={this.state.number} onClick={this.increment}>{this.state.number}</button></div>;
    }
}

class MajorBlock extends React.Component {

    constructor(props) {
        super(props);
        this.numbers = props.numbers;
    }

    render() {
        let minorBlocks = Array(9).fill(0).map((value, index) => <MinorBlock key={index} number={this.numbers[index]} />);
        return <div className="MajorBlock">{minorBlocks}</div>
    }
}

export class Board extends React.Component {

    constructor() {
        super();
        this.numbers = new Array(9).fill(new Array(9).fill(null));
    }

    render() {
        let majorBlocks = Array(9).fill(0).map((value, index) => < MajorBlock key={index} numbers={this.numbers[index]} />);
        return (<div className="Board">{majorBlocks}</div>);
    }
}
